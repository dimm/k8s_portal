{{define "body"}}
  {{if not .User.Name}}
  <div class="container mt-5">
      <div class="jumbotron">
              <h1 class="display-5">PRP Kubernetes portal</h1>
	      <p class="lead">Here you can get an account in Pacific Research Platform kubernetes portal by <a href="/auth">logging in</a> with your university's credentials and requesting access in <a href="https://rocket.nautilus.optiputer.net">RocketChat</a></p>
	      <p class="lead">You can easily join your node in our cluster - request instructions in RocketChat #general channel.</p>
      </div>
  </div>
  {{end}}
  {{if .User.Name}}
  <div class="container">
      <div class="jumbotron">
        <div class="container">
          <div class="jumbotron">
            <h3>Support channel</h3>
            <p>We provide support in RocketChat: <a href="https://rocket.nautilus.optiputer.net">https://rocket.nautilus.optiputer.net</a><br/>
            You can install a client application: <a href="https://rocket.chat/install">https://rocket.chat/install</a> or use the web version.</p>

            <h3>Wiki</h3>
            <p><a href="https://wiki.nautilus.optiputer.net">https://wiki.nautilus.optiputer.net</a></p>

          {{if eq .User.Spec.Role "admin"}}
            <h3>Admin interface</h3>
            <p>You were granted the admin role in the cluster. You can create your own namespaces, run PODs in those and add users.<br/>
              To add a new namespace, click "Manage namespaces" in top menu. Then you can add other users to it.
              New registered users should be verified in order to be eligible for addition to namespace. To verify a new user, go to Users tab. Please only verify users known to you.
            </p>
            {{end}}

          {{if eq .User.Spec.Role "user"}}
              <h3>Using the cluster</h3>
              <p>You were granted the user role in the cluster. Once an owner of a namespace adds you to his namespace, you will get access to all namespace resources. To get access to a namespace, please contact its owner.
              </p>
          {{end}}

          {{if eq .User.Spec.Role "guest"}}
            <h3>Getting verified</h3>
            <p>Welcome to nautilus cluster. Currently your role is Guest.<br/>
            If you would like to join any existing project/namespace, contact its administrator to add you to it. You will get the User role.<br/>
            If you would like to start your own project, you can login to RocketChat and request being promoted to Admin. This will allow you to create your own namespaces and invite other users into those.
            </p>
            {{end}}

            <h3>PRP Kubernetes quick start</h3>
            <ol>
            <li><a href="https://kubernetes.io/docs/tasks/tools/install-kubectl/">Install</a> the kubectl tool</li>
            <li>Click the "Get Config" link on top and get your config file</li>
              <li>Put the file to your &lt;home&gt;/.kube folder</li>
              <li>Make sure you're promoted from guest, and have a namespace assigned to you</li>
              <li>Test kubectl can connect to the cluster: <code>kubectl get pods -n <i>your_namespace</i></code>. It's possible there are no pods in your namespace yet.</li>
              <li>Run busybox container in your namespace: <code>kubectl run busybox -n <i>your_namespace</i> -it --rm --image=busybox -- sh</code>. It will quit once you log out from the console.</li>
              <li><a href="https://kubernetes.io/docs/tutorials/kubernetes-basics/deploy-intro/">Learn</a> more about kubernetes.</li>
              <li><a href="https://kubernetes.io/docs/reference/kubectl/cheatsheet/">Get familiar</a> with kubectl tool.</li>
            </ol>

            <h5>Limits</h5>
            <p>The default <a href="https://kubernetes.io/docs/tasks/configure-pod-container/assign-memory-resource/#specify-a-memory-request-and-a-memory-limit">Memory limit</a> per container for most namespaces is 4Gi. You can increase it for a container if needed.</p>

            <h5>Running GPU PODs</h5>
            <p>Use the <a href="https://gitlab.com/dimm/prp_k8s_config/blob/master/tensorflow-example.yaml">tensorflow example POD</a> definition to create your own pod and deploy it to kubernetes.</p>
            <p>You can try running this example in your namespace with:
              <div><code>kubectl create -n <i>your_namespace</i> -f https://gitlab.com/dimm/prp_k8s_config/raw/master/tensorflow-example.yaml</code></div>
            <br/>
            <p>and destroy with</p>
            <div><code>kubectl delete -n <i>your_namespace</i> -f https://gitlab.com/dimm/prp_k8s_config/raw/master/tensorflow-example.yaml</code></div><br/>
            This example requests 1 GPU device. You can have up to 8 per node.
            If you request GPU devices in your POD, kubernetes will auto schedule your pod to the appropriate node. There's no need to specify the location manually.
            <p>To request NVIDIA Tesla K40c GPUs use <a href="https://gitlab.com/dimm/prp_k8s_config/blob/master/tensorflow-k40.yaml">this example</a></p>
            <b><i>You should delete your PODs when your computation is done to let other users use the GPU. Consider using <a href="https://kubernetes.io/docs/concepts/workloads/controllers/jobs-run-to-completion/">Jobs</a> when possible to ensure your POD is not wasting GPU time.</i></b><br/>
            
            <br/><br/>
            <h5>Attaching Ceph blockdevice</h5>
            <p>Use the <a href="https://gitlab.com/dimm/prp_k8s_config/blob/master/volume-example.yaml">volume example POD</a> definition to create your own pod with <a href="https://grafana.nautilus.optiputer.net/dashboard/db/rook-ceph">Rook block volume</a> attached.</p>
            <h5>Running MPI jobs</h5>
            <p>Follow the steps in <a href="https://gitlab.com/dimm/prp_k8s_config/tree/master/mpi">https://gitlab.com/dimm/prp_k8s_config/tree/master/mpi</a></p>
          </div>
        </div>
      </div>
  </div>
  {{end}}
{{end}}

{{define "page_css"}}
<style type="text/css">
  code {
      word-wrap: break-word;
  }
</style>
{{end}}
